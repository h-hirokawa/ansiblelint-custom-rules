import re
from pathlib import Path

from ansiblelint.rules import AnsibleLintRule

class S002_FilenameCheck(AnsibleLintRule):
    id = 'S002'
    shortdesc = '特定のファイル名を禁止する'
    description = 'ファイル名を正規表現でチェックするサンプル実装。'
    severity = 'MEDIUM'
    tags = ['must']
    version_added = 'v4.3.7'

    tested = set() 

    def match(self, file, text):
        if file['type'] != 'playbook':
            return False

        path = file['path']
        name = Path(file["path"]).stem

        #最後にcopyを禁止
        if re.match(r'.*copy$', name) and path not in self.tested:
            self.tested.add(path)
            return True

        #最後に年号を禁止
        if re.match(r'.*202[0|1]$', name) and path not in self.tested:
            self.tested.add(path)
            return True

        return False
