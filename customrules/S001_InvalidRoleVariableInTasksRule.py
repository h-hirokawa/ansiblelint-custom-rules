import re
from pathlib import Path
from itertools import chain
from typing import TYPE_CHECKING, List

from ansiblelint.rules import AnsibleLintRule
from ansiblelint.utils import parse_yaml_from_file

# create_matcherrorはまだメインで実装(v4.3.7時点)されてない。5.0で実装予定。
# 5.0になったら削除可能
# copy from https://github.com/ansible-community/ansible-lint/blob/f25dcc2e963d2df25fe68a053c8b26dca02e47ec/src/ansiblelint/rules/__init__.py
import copy
from typing import Iterator, List, Optional, Union
from ansiblelint.errors import MatchError

def create_matcherror(
        self,
        message: Optional[str] = None,
        linenumber: int = 0,
        details: str = "",
        filename: Optional[Union[str]] = None,
        tag: str = "") -> MatchError:
    match = MatchError(
        message=message,
        linenumber=linenumber,
        details=details,
        filename=filename,
        rule=copy.copy(self)
        )
    if tag:
        match.tag = tag
    return match

AnsibleLintRule.create_matcherror = create_matcherror
# ここまで

class S001_InvalidRoleVariableInTasksRule(AnsibleLintRule):
    id = 'S001'
    shortdesc = 'Role間衝突を防ぐためにRole名をPrefixとすることが一般的'
    description = 'Role間衝突を防ぐためにRole名をPrefixとすることが一般的'
    severity = 'MEDIUM'
    tags = ['must']
    version_added = 'v4.3.7'

    checked_roles = set()

    # 判定条件関数
    def is_valid_role_var(self, var_name, role_name):
        # <role_name>_xxx_yyy って変数であることを確認
        if not var_name.startswith(role_name+'_'):
            return False

        # 変数名に特定の正規表現で制限する場合は以下をアンコメント(アンダースコアを禁止するパターン)
        #if not re.match(r"^[a-zA-Z0-9]+$", var_name):
        #    return False
        return True

    def _remove_prefix(self, text, prefix):
        return re.sub(r'^{0}'.format(re.escape(prefix)), '', text)

    def matchyaml(self, file, text):
        path = file['path'].split("/")
        results = []
        if "tasks" in path:
            role_name = self._remove_prefix(path[path.index("tasks") - 1], "ansible-role-")
            role_root = path[:path.index("tasks")]
            defaults_dir = Path("/".join(role_root)) / "defaults"
            vars_dir = Path("/".join(role_root)) / "vars"

            # 一度確認したroleは除外
            if Path("/".join(role_root)) in self.checked_roles:
                return False
            else:
                self.checked_roles.add(Path("/".join(role_root)))

            yaml_paths = []
            if defaults_dir.exists() and defaults_dir.is_dir():
                yaml_paths.extend(
                    [
                        defaults_dir.glob("*.yml"),
                        defaults_dir.glob("*.yaml"),
                        defaults_dir.glob("*.json"),
                    ]
                )
            if vars_dir.exists() and vars_dir.is_dir():
                yaml_paths.extend(
                    [
                        vars_dir.glob("*.yml"),
                        vars_dir.glob("*.yaml"),
                        vars_dir.glob("*.json"),
                    ]
                )
            
            for path in chain(*yaml_paths):
                yaml_data = parse_yaml_from_file(str(path))
                
                for k, v in yaml_data.items():
                    lineNumber = k.ansible_pos[1]
                    if not self.is_valid_role_var(k, role_name):
                        results.append(
                            self.create_matcherror(
                                message=self.shortdesc,
                                linenumber=lineNumber,
                                details=k,
                                filename=str(path),
                            )
                        )
        return results