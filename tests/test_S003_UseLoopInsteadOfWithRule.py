from customrules.S003_UseLoopInsteadOfWithRule import (
    S003_UseLoopInsteadOfWithRule
)
from tests import TestBase


class TestS003_UseLoopInsteadOfWithRule(TestBase):

    def setUp(self):
        self._register_rule(S003_UseLoopInsteadOfWithRule())

    def test_success(self):
        self._assert_success_yamls("s003_success")

    # テストで何個失敗するか記述
    def test_fail(self):
        self._assert_fail_yamls("s003_fail", 1)
