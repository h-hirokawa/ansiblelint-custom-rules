"""Taken from https://raw.githubusercontent.com/ansible/ansible-lint/\
9ecaa5e60bdb46de84735ad4674b286cff17463a/test/__init__.py"""
import tempfile
import shutil
import os
import unittest

from pathlib import Path
from ansiblelint.rules import RulesCollection
from ansiblelint.runner import Runner

PLAYBOOKS_DIR = Path(__file__).absolute().parent / "test_playbooks"


class TestBase(unittest.TestCase):
    """Base class with utils."""

    def _register_rule(self, rule, verbosity=0):
        self.collection = RulesCollection()
        self.collection.register(rule)
        self.__verbosity = verbosity

    def __assert_yamls(self, dir_name, num):
        path = PLAYBOOKS_DIR / dir_name
        if not path.exists():
            raise IOError("No such a directory or a file: {}".format(str(path)))
        yamls = path.glob("*.yml")
        results = []
        for y in yamls:
            results.extend(
                Runner(self.collection, str(y), [], [], [], self.__verbosity).run()
            )
        self.assertEqual(len(results), num)

    def _assert_fail_yamls(self, dir_name, num=1):
        self.__assert_yamls(dir_name, num)

    def _assert_success_yamls(self, dir_name):
        self.__assert_yamls(dir_name, 0)

    def __assert_path(self, path_name, num):
        path = PLAYBOOKS_DIR / path_name
        if not path.exists():
            raise IOError("No such a file or a directory: {}".format(str(path)))
        results = Runner(self.collection, str(path), [], [], [], self.__verbosity).run()
        self.assertEqual(len(results), num)

    def _assert_fail_path(self, path_name, num=1):
        self.__assert_path(path_name, num)

    def _assert_success_path(self, path_name):
        self.__assert_path(path_name, 0)


class RunFromText(object):
    """Use Runner on temp files created from unittest text snippets."""

    def __init__(self, collection):
        self.collection = collection

    def _call_runner(self, path):
        runner = Runner(self.collection, path, [], [], [])
        return runner.run()

    def run_playbook(self, playbook_text, filename="playbook.yml", play_root=None):
        if not play_root:
            play_root = tempfile.mkdtemp()
        with open(os.path.join(play_root, filename), "w") as fp:
            fp.write(playbook_text)
        results = self._call_runner(fp.name)
        if not play_root:
            shutil.rmtree(play_root)
        return results

    def run_role_tasks_main(self, tasks_main_text):
        role_path = tempfile.mkdtemp(prefix="role_")
        tasks_path = os.path.join(role_path, "tasks")
        os.makedirs(tasks_path)
        with open(os.path.join(tasks_path, "main.yml"), "w") as fp:
            fp.write(tasks_main_text)
        results = self._call_runner(role_path)
        shutil.rmtree(role_path)
        return results

    def run_role_meta_main(self, meta_main_text):
        role_path = tempfile.mkdtemp(prefix="role_")
        meta_path = os.path.join(role_path, "meta")
        os.makedirs(meta_path)
        with open(os.path.join(meta_path, "main.yml"), "w") as fp:
            fp.write(meta_main_text)
        results = self._call_runner(role_path)
        shutil.rmtree(role_path)
        return results
