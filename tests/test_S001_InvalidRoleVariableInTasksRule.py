from customrules.S001_InvalidRoleVariableInTasksRule import (
    S001_InvalidRoleVariableInTasksRule
)
from tests import TestBase


class TestS001_InvalidRoleVariableInTasksRule(TestBase):

    def setUp(self):
        self._register_rule(S001_InvalidRoleVariableInTasksRule())

    def test_success(self):
        self._assert_success_path("s001_success/roles/sample_role/")

    # テストで何個失敗するか記述
    def test_fail(self):
        self._assert_fail_path("s001_fail/roles/sample_role/", 6)
