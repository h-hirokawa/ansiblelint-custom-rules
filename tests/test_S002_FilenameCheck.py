from customrules.S002_FilenameCheck import (
    S002_FilenameCheck
)
from tests import TestBase


class TestS002_FilenameCheck(TestBase):

    def setUp(self):
        self._register_rule(S002_FilenameCheck())

    def test_success(self):
        self._assert_success_yamls("s002_success")

    # テストで何個失敗するか記述
    def test_fail(self):
        self._assert_fail_yamls("s002_fail", 2)
