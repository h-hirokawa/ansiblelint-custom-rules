# ansible-lint-custom-rules
Ansible Lintカスタムルール, yamllintカスタムconfig

# 作成・検証環境
Python 3.9.1  
ansible-lint 4.3.7  
yamllint 1.25.0  

# 使用方法
`ansible-lint` `yamllint` インストール済み環境に当リポジトリを配置し、下記の様に実行してください。
```
ansible-lint -R -r /path/to/ansible-lint-custom-rules/customrules tests/test_playbooks/s002_fail/*
yamllint -c /path/to/ansible-lint-custom-rules/yamllint_config tests/test_playbooks/yamlint001.yml
```

# 実装ルール(ansible-lint)
`ansible-lint -r /path/to/ansible-lint-custom-rules/ -L` コマンドにて出力される内容
```
# ansible-lint -r customrules -L
  S001             │ Role間衝突を防ぐためにRole名をPrefixとすることが一般的                                         
╶──────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────╴
  description      │ Role間衝突を防ぐためにRole名をPrefixとすることが一般的                                         
  version_added    │ v4.3.7                                                                                         
  tags             │ must                                                                                           
  severity         │ MEDIUM                                                                                         
                   ╵                                                                                                
                   ╷                                                                                                
  S003             │ Playbook中の繰り返し表現には公式推奨である loop 記法を使用                                     
╶──────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────╴
  description      │ Playbook中の繰り返し表現には公式推奨である loop 記法を使用                                     
                   │ 抽出条件は公式ドキュメントに変換例があるもの https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#migrating-from-with-x-to-loop
  version_added    │ v4.3.7                                                                                         
  tags             │ must                                                                                           
  severity         │ MEDIUM                                                                                         
                   ╵                                                                                                
                   ╷                                                                                                
  S005             │ 特定のファイル名を禁止する                                                                     
╶──────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────╴
  description      │ ファイル名を正規表現でチェックするサンプル実装。                                              
  version_added    │ v4.3.7                                                                                         
  tags             │ must                                                                                           
  severity         │ MEDIUM                                                                                         
                   ╵                              
```
# 実装ルール(yamllint)
yamllint_config より一部抜粋  
```
# 改行コード制御
  # POSIX標準では、最後の行を改行文字で終了する必要がある
  new-line-at-end-of-file: enable

  # 改行コードがLF(\n)であることを確認
  new-lines:
    type: unix

# ブロック記法制御
  # 中括弧(braces) "{" と "}"によるFlow Mappingの禁止.
  braces:
    forbid: true

  # 角括弧(brackets)"[" と "]"によるflow sequencesの禁止
  brackets:
    forbid: true

# その他よく設定されているもの
  # 一行の文字数の制限のルール緩和
  line-length:
    max: 120
```

# ansible-lint カスタムルールテスト方法
```
python3 -m venv ansible-lint-custom-rules 
cd ansible-lint-custom-rules 
source bin/activate
pip3 install ansible-lint yamllint pytest 
pytest ./tests
# tests/test_playbooks 以下にあるs00*サンプルコードに対してテストを実施
```

# License
MIT

# Author Information
Copyright (C) 2021 Red Hat, Inc..